{ mkDerivation, aeson, base, base58-bytestring, binary, bytestring
, conduit, crypto-api, cryptohash-sha256, entropy, http-conduit
, stdenv, text, time, unordered-containers
}:
mkDerivation {
  pname = "oauth2-simple";
  version = "0.1.1.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base base58-bytestring binary bytestring conduit crypto-api
    cryptohash-sha256 entropy http-conduit text time
    unordered-containers
  ];
  description = "A simple OAuth2 library";
  license = stdenv.lib.licenses.bsd3;
}
